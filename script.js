let button = document.querySelector("#btn-enviar")
button.addEventListener("click", enviarMensagem)

let body_chat = document.querySelector("#messages")

let message_box = document.querySelector("#message-box")

var contador = 0
function deletarMensagem(counter) {
    document.getElementById(counter).remove();
}

function enviarMensagem() { 
    if (message_box.value != "") {
        let div_p = document.createElement("div")
        let div_messages = document.createElement("div")
        let p = document.createElement("p")
        let delete_button = document.createElement("button")
        let edit_button = document.createElement("button")

        div_p.classList.add("div-paragrafo")
        div_messages.setAttribute("id", contador)
        delete_button.classList.add("button", "delete-button")
        edit_button.classList.add("button", "edit-button")



        delete_button.innerHTML = "Excluir"
        edit_button.innerHTML = "Editar"
        
        let mensagem = message_box.value
        p.innerHTML = mensagem

        div_p.append(p)
        div_messages.append(div_p)
        div_messages.append(delete_button)
        div_messages.append(edit_button)
        body_chat.append(div_messages)

        message_box.value = ""
        
        
        // deletar mensagem
        delete_button.setAttribute("onclick", "deletarMensagem("+contador+")")
    }
    
}





